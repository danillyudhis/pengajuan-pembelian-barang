/*
 Navicat Premium Data Transfer

 Source Server         : localdb
 Source Server Type    : MySQL
 Source Server Version : 50733
 Source Host           : localhost:3306
 Source Schema         : ci_barang

 Target Server Type    : MySQL
 Target Server Version : 50733
 File Encoding         : 65001

 Date: 07/06/2024 16:54:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pengajuan
-- ----------------------------
DROP TABLE IF EXISTS `pengajuan`;
CREATE TABLE `pengajuan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_photo` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `deskripsi_barang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `harga_barang` decimal(65, 0) NULL DEFAULT NULL,
  `kuantity_barang` int(10) NULL DEFAULT NULL,
  `link_pembelian_produk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `link_video_produk` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tahap_pengajuan` enum('manager','finance') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `reject_reason` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `manager_approval_status` enum('approve','rejected') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `finance_approval_status` enum('approve','rejected') CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `finance_transfer_approval_photo` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `manager_approval_status_timestamp` int(100) NULL DEFAULT NULL,
  `finance_approval_status_timestamp` int(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pengajuan
-- ----------------------------
INSERT INTO `pengajuan` VALUES (4, 'PSA JAKL', 'https://images.guns.com/prod/2024/05/31/6659c2b11aa0964286d893be1b1bef58ff3de64b5c80a.jpg?imwidth=1200', '', 21020857, 2, 'https://www.guns.com/firearms/handguns/semi-auto/palmetto-state-armory-jakl-5-56x45mm-nato-30-rounds-10-5-barrel-used?p=1158159', 'https://youtu.be/gqhJnhvpKd0?si=jZ0qKlZvGmKc9eRt&t=287', 'finance', 'ugly', 'approve', 'rejected', NULL, 1717748433, 1717748433);
INSERT INTO `pengajuan` VALUES (6, 'GPNVG-18', 'https://tnvc.com/wp-content/webp-express/webp-images/doc-root/wp-content/uploads/2021/01/GPNVG_1.jpg.webp', '', 688689652, 2, 'https://darqindustries.com/products/l3harris-gpnvg-18', 'https://www.youtube.com/watch?v=XmDKgiSRIgQ&t=141s', 'finance', NULL, 'approve', 'approve', '08d7e0bbe80f8a3ddfe658cf685b685c.png', 1717748433, 1717748433);
INSERT INTO `pengajuan` VALUES (7, 'GLOCK 47', 'https://academy.scene7.com/is/image/academy/21068548?$pdp-gallery-ng$', '', 10193237, 1, 'https://www.academy.com/p/glock-g47-gen-5-9mm-mos-semiautomatic-pistol', 'https://www.youtube.com/watch?v=0NL1sLvV94s&t=24s', 'manager', 'Not suitable for current issue', 'rejected', NULL, NULL, NULL, NULL);
INSERT INTO `pengajuan` VALUES (8, 'EOTECH EXPS3', 'https://images.guns.com/prod/2021/09/22/614b4044857bf0366f47713abb8ae40f0450e7f0bcfe2.jpeg?imwidth=1200', '', 12044850, 1, 'https://www.guns.com/optics/red-dot-scopes/eotech-exps3-1x-30x23mm-1-moa-new?p=49802', 'https://youtu.be/x8qympAk-HA?si=KXTS_CzjyHFSpgkO&t=127', 'finance', NULL, 'approve', NULL, NULL, 1717748433, NULL);
INSERT INTO `pengajuan` VALUES (9, 'EOTECH EXPS3 Duplicate', 'https://images.guns.com/prod/2021/09/22/614b4044857bf0366f47713abb8ae40f0450e7f0bcfe2.jpeg?imwidth=1200', NULL, 12044850, 1, 'https://www.guns.com/optics/red-dot-scopes/eotech-exps3-1x-30x23mm-1-moa-new?p=49802', 'https://www.youtube.com/watch?si=KXTS_CzjyHFSpgkO&t=127&v=x8qympAk-HA&feature=youtu.be', 'manager', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `pengajuan` VALUES (10, 'KASET RAN', 'https://images.tokopedia.net/img/cache/500-square/VqbcmM/2024/3/23/b9e51258-cbcb-45db-bafd-061385834399.jpg.webp?ect=4g', NULL, 45000, 1, 'https://www.tokopedia.com/harihfa/kaset-ran?extParam=ivf%3Dfalse%26src%3Dsearch', 'https://www.youtube.com/watch?v=DeWlw6jn-qw', 'finance', NULL, 'approve', NULL, NULL, 1717748433, NULL);
INSERT INTO `pengajuan` VALUES (11, 'McLaren 720S Coupe', 'https://apollo.olx.co.id/v1/files/665746bcbf0e2-ID/image;s=780x0;q=60', NULL, 7750000000, 1, 'https://www.olx.co.id/olxmobbi/item/mclaren-720s-coupe-2018-memphis-red-iid-921135989', 'https://www.youtube.com/watch?v=qONckKNrkT8', 'manager', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `no_telp` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role` enum('officer','manager','finance') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `foto` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (7, 'Hapag-Lioyd', 'arfandotid', 'arfandotid@gmail.com', '081221528805', 'finance', '$2y$10$S.RCMP7k.qM4OB9sILw3KO2A448.e6yQjWH1//nR56jJki5Ov48QC', 1568691611, '58b74ae648dab7f7689f6c479d53b164.png', 1);
INSERT INTO `user` VALUES (8, 'Maersk line', 'mghifariarfan', 'mghifariarfan@gmail.com', '085697442673', 'manager', '$2y$10$S.RCMP7k.qM4OB9sILw3KO2A448.e6yQjWH1//nR56jJki5Ov48QC', 1568691629, 'user.png', 1);
INSERT INTO `user` VALUES (13, 'Danill Yudhistira', 'arfankashilukato', 'arfankashilukato@gmail.com', '081623123181', 'officer', '$2y$10$S.RCMP7k.qM4OB9sILw3KO2A448.e6yQjWH1//nR56jJki5Ov48QC', 1569192547, 'user.png', 1);

ALTER TABLE `user` ADD UNIQUE(`username`);

SET FOREIGN_KEY_CHECKS = 1;
