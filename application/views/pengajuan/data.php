<?= $this->session->flashdata('pesan'); ?>
<div class="card shadow-sm border-bottom-primary">
    <div class="card-header bg-white py-3">
        <div class="row">
            <div class="col">
                <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                    Pengajuan Pembelian
                </h4>
            </div>
            <?php if($session["role"] == "officer") { ?>
                <div class="col-auto">
                    <a href="<?= base_url('pengajuan/add') ?>" class="btn btn-sm btn-primary btn-icon-split">
                        <span class="icon">
                            <i class="fa fa-plus"></i>
                        </span>
                        <span class="text">
                            Tambah Pengajuan Pembelian
                        </span>
                    </a>
                </div>
            <?php } ?>
            <?php if($session["role"] == "manager") { ?>
                <div class="col-auto">
                    <a href="<?= base_url('pengajuan/history_pengajuan') ?>" class="btn btn-sm btn-primary btn-icon-split">
                        <span class="icon">
                            <i class="fa fa-clock"></i>
                        </span>
                        <span class="text">
                            History Approval Pengajuan Pembelian
                        </span>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-striped w-100 dt-responsive nowrap" id="dataTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Image</th>
                    <th>Nama Barang</th>
                    <th>Harga Barang Satuan</th>
                    <th class="text-center">Kuantity Barang</th>
                    <th>Total Harga Barang</th>
                    <th class="text-center">Link Pembelian Barang</th>
                    <th class="text-center">Link Video Produk</th>
                    <?php if($session["role"] == "officer" || $session["role"] == "finance") { ?>
                        <th>Status Pengajuan</th>
                    <?php } ?>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($pengajuan) :
                    $no = 1;
                    foreach ($pengajuan as $s) :
                        ?>
                        <tr>
                            <td><?= $no++; ?></td>
                            <td> <img src="<?= $s['link_photo']; ?>" style="max-width: 100%; width: 100px;" /></td>
                            <td><?= $s['nama_barang']; ?></td>
                            <td>Rp <?= number_format($s['harga_barang'], 2, ",", "."); ?></td>
                            <td style="text-align: center;"><?= $s['kuantity_barang']; ?></td>
                            <td>Rp <?= number_format($s['harga_barang'] * $s['kuantity_barang'], 2, ",", "."); ?></td>
                            <td class="text-center">
                                <?php if(!empty(trim($s['link_pembelian_produk']))) { ?>
                                    <a href="<?= $s['link_pembelian_produk']; ?>" target="_blank">Klik Disini</a> 
                                <?php } ?>
                            </td>
                            <td class="text-center">
                                <?php if(!empty(trim($s['link_video_produk']))) { ?>
                                    <a href="<?= $s['link_video_produk']; ?>" target="_blank">Klik Disini</a> 
                                <?php } ?>
                            </td>
                            <?php if($session["role"] == "officer" || $session["role"] == "finance") { ?>
                                <td>
                                    <?php if($s["manager_approval_status"] == "rejected" || $s["finance_approval_status"] == "rejected") { ?>
                                        <?php if($s["manager_approval_status"] == "rejected") { ?>
                                            <div>
                                                Pengajuan di tolak oleh manager
                                                <?php if(!empty($s["manager_approval_status_timestamp"])) {?>
                                                    <?= date("d/m/Y H:i:s", $s["manager_approval_status_timestamp"]) ?>
                                                <?php } ?>
                                            </div>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rejectedReason" data-title="<?= $s['reject_reason']; ?>" data-content="<?= $s['reject_reason'] ?>">Rejected Reason</button>
                                        <?php } ?> 
                                        <?php if($s["finance_approval_status"] == "rejected") { ?>
                                            <div>
                                                Pengajuan di tolak oleh finance
                                                <?php if(!empty($s["finance_approval_status_timestamp"])) {?>
                                                    <?= date("d/m/Y H:i:s", $s["finance_approval_status_timestamp"]) ?>
                                                <?php } ?>
                                            </div>
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#rejectedReason" data-title="<?= $s['reject_reason']; ?>" data-content="<?= $s['reject_reason'] ?>">Rejected Reason</button>
                                        <?php } ?> 
                                    <?php } else if($s["manager_approval_status"] == "approve" && $s["finance_approval_status"] == "approve") { ?>
                                        <div>
                                            Pengajuan disetujui manager & finance
                                            <?php if(!empty($s["finance_approval_status_timestamp"])) {?>
                                                <?= date("d/m/Y H:i:s", $s["finance_approval_status_timestamp"]) ?>
                                            <?php } ?>
                                        </div>
                                        <a class="btn btn-success" target="_blank" href="<?= base_url(); ?>uploads/<?= $s["finance_transfer_approval_photo"] ?>">Bukti Transfer</a>
                                    <?php } else { ?>
                                        Menunggu persetujuan <?= $s['tahap_pengajuan'] ?>
                                    <?php } ?>
                                </td>
                            <?php } ?>
                            <th>
                                <?php if($session["role"] == "manager") { ?>
                                    <?php if(empty($s["finance_approval_status"])) { ?>
                                        <?php } ?>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" data-title="<?= $s['nama_barang']; ?>" data-id="<?= $s['id']; ?>" data-content="<?= $s['nama_barang'] . " - "; ?>">Reject</button>
                                        <a class="btn btn-primary text-white" href="<?= base_url("pengajuan/approve/" . $s['id'] . "?nama=" . $s['nama_barang']) ?>">Approve</a>
                                <?php } ?>

                                <?php if($session["role"] == "finance") {?>
                                    <?php if(empty($s["finance_approval_status"])) { ?>
                                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal" data-title="<?= $s['nama_barang']; ?>" data-id="<?= $s['id']; ?>" data-content="<?= $s['nama_barang'] . " - "; ?>">Reject</button>
                                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#approvalFinance" data-title="<?= $s['nama_barang']; ?>" data-id="<?= $s['id']; ?>" data-content="<?= $s['nama_barang'] . " - "; ?>">Approve</button>
                                    <?php } ?>
                                <?php } ?>

                                <?php if($session["role"] == "officer") { ?>
                                    <!-- setelah di approve / di reject manager maka data tidak bisa di edit atau di hapus -->
                                    <?php if( empty($s["manager_approval_status"]) && empty($s["finance_approval_status"]) ) { ?>
                                        <a href="<?= base_url('pengajuan/edit?id=') . $s['id'] ?>" class="btn btn-circle btn-warning btn-sm"><i class="fa fa-edit"></i></a>
                                        <a onclick="return confirm('Yakin ingin hapus?')" href="<?= base_url('pengajuan/delete/') . $s['id'] ?>" class="btn btn-circle btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                    <?php } ?>
                                <?php } ?>
                            </th>
                        </tr>
                    <?php endforeach; ?>
                <?php else : ?>
                    <tr>
                        <td colspan="7" class="text-center">
                            Data Kosong
                        </td>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    </div>
</div>

<script src="<?= base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>

<script>
$(document).ready(function() {
    $('#rejectedReason').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget); // Button that triggered the modal
        let title = button.data('title'); // Extract info from data-* attributes
        let id = button.data('id');
        let content = button.data('content');

        // Update the modal's content.
        let modal = $(this);
        // modal.find('.modal-title').text('Reject '.concat(title));
        // modal.find('.id-input').val(id);
        modal.find('.modal-body').text(content);
    });
});
</script>
<div class="modal fade" id="rejectedReason" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reject Reason</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>

<?php if($session["role"] == "manager" || $session["role"] == "finance") { ?>
<script>
$(document).ready(function() {
    $('#exampleModal').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget); // Button that triggered the modal
        let title = button.data('title'); // Extract info from data-* attributes
        let id = button.data('id');
        // let content = button.data('content');

        // Update the modal's content.
        let modal = $(this);
        modal.find('.modal-title').text('Reject '.concat(title));
        modal.find('.id-input').val(id);
        // modal.find('.modal-body').text(content);
    });
});
</script>
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Default Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            <?= form_open('pengajuan/reject', []) ?>

            <div class="row form-group">
                <label class="col-md-3 text-md-right" for="alasan">Alasan</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <input type="hidden" value="" class="id-input" name="id_pengajuan">
                        <textarea name="alasan" class="form-control" required></textarea>
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-9 offset-md-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </div>

            <?= form_close(); ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php if($session["role"] == "finance") { ?>
<script>
$(document).ready(function() {
    $('#approvalFinance').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget); // Button that triggered the modal
        let title = button.data('title'); // Extract info from data-* attributes
        let id = button.data('id');

        // Update the modal's content.
        let modal = $(this);
        modal.find('.modal-title').text('Approve '.concat(title));
        modal.find('.id-input').val(id);
        modal.find('.nama_barang').val(title);
        // modal.find('.modal-body').text(content);
    });
});
</script>
<div class="modal fade" id="approvalFinance" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Default Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            <?= form_open_multipart('pengajuan/finance_approve'); ?>

            <div class="row form-group">
                <label class="col-md-3 text-md-right" for="alasan">Photo Bukti Transfer</label>
                <div class="col-md-9">
                    <div class="input-group">
                        <input type="hidden" value="" class="id-input" name="id_pengajuan">
                        <input type="hidden" value="" class="nama_barang" name="nama_barang">
                        <!-- <textarea name="alasan" class="form-control" required></textarea> -->
                        <div class="row form-group">
                            <!-- <label class="col-md-3 text-md-right" for="alasan">Photo Bukti Transfer</label>
                            <div class="col-md-9">    
                            </div> -->
                            <div class="input-group">
                                    <input type="file" name="userfile" accept="image/*" onchange="loadFile(event)" required>
                                    <img id="output" style="max-width: 100%; width: 300px; margin-top: 10px;"/>
                                    <script>
                                    let loadFile = function(event) {
                                        let output = document.getElementById('output');
                                        output.src = URL.createObjectURL(event.target.files[0]);
                                        output.onload = function() {
                                            URL.revokeObjectURL(output.src) // free memory
                                        }
                                    };
                                    </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-md-9 offset-md-3">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </div>

            <?= form_close(); ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
            </div>
        </div>
    </div>
</div>
<?php } ?>