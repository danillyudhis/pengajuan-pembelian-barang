
<script src="https://code.jquery.com/jquery-3.7.1.slim.min.js" integrity="sha256-kmHvs0B+OpCW5GVHUNjv9rOmY0IvSIRcf7zGUDTDQM8=" crossorigin="anonymous"></script>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card shadow-sm border-bottom-primary">
            <div class="card-header bg-white py-3">
                <div class="row">
                    <div class="col">
                        <h4 class="h5 align-middle m-0 font-weight-bold text-primary">
                            Form Pengajuan Pembelian Barang
                        </h4>
                    </div>
                    <div class="col-auto">
                        <a href="<?= base_url('Pengajuan') ?>" class="btn btn-sm btn-secondary btn-icon-split">
                            <span class="icon">
                                <i class="fa fa-arrow-left"></i>
                            </span>
                            <span class="text">
                                Kembali
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <?= $this->session->flashdata('pesan'); ?>
                <?= form_open_multipart(''); ?>

                <?php // form_open('', []) ?>

                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="nama_barang">Nama Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input value="" name="nama_barang" type="text" class="form-control" placeholder="Nama Barang" required>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="nama_barang">Photo (Opsional)</label>
                    <div class="col-md-9">
                        <!-- <div class="input-group">
                            <input value="" name="link_photo" type="text" class="form-control" placeholder="Link Photo">
                        </div> -->
                        <div class="input-group">
                                <input type="file" name="photo_file" accept="image/*" onchange="loadFile(event)">
                                <img id="output" style="max-width: 100%; width: 400px;"/>
                                <script>
                                let loadFile = function(event) {
                                    let output = document.getElementById('output');
                                    output.src = URL.createObjectURL(event.target.files[0]);
                                    output.onload = function() {
                                        URL.revokeObjectURL(output.src) // free memory
                                    }
                                };
                                </script>
                        </div>

                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="harga_barang">Harga Satuan Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input value="" name="harga_barang" type="number" class="form-control" placeholder="Harga Satuan Barang" required>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="kuantity_barang">Kuantity Barang</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input value="" name="kuantity_barang" type="number" class="form-control" placeholder="Kuantity Barang" required>
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="link_video_produk">Link Video Produk (Opsional)</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input value="" name="link_video_produk" type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=FAOlpVuSeWI&list=RDEaJV2irv7MU&index=2" >
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <label class="col-md-3 text-md-right" for="link_pembelian_produk">Link Pembelian Produk (Opsional)</label>
                    <div class="col-md-9">
                        <div class="input-group">
                            <input value="" name="link_pembelian_produk" type="text" class="form-control" placeholder="https://www.youtube.com/watch?v=FAOlpVuSeWI&list=RDEaJV2irv7MU&index=2" >
                        </div>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-9 offset-md-3">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                        <button type="reset" class="btn btn-secondary">Reset</button>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>