<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajuan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        cek_login();

        $this->load->model('Pengajuan_model', 'pengajuan');
        $this->load->library('form_validation');

        // $ci->session->userdata('login_session')['role']
        $this->ci = get_instance();
    }

    public function index()
    {

        $list = array();
        if($this->ci->session->userdata('login_session')["role"] == "manager") {
            $list = $this->pengajuan->where_data("pengajuan", ["tahap_pengajuan" => "manager", "manager_approval_status" => null]);
        }

        if($this->ci->session->userdata('login_session')["role"] == "finance") {
            $list = $this->pengajuan->where_data("pengajuan", ["tahap_pengajuan" => "finance"]);
        }

        if($this->ci->session->userdata('login_session')["role"] == "officer") {
            $list = $this->pengajuan->where_data("pengajuan", []);
        }

        $data['title'] = "PENGAJUAN";
        $data['pengajuan'] = $list;

        $data['session'] = $this->ci->session->userdata('login_session');
        $this->template->load('templates/dashboard', 'pengajuan/data', $data);
    }

    public function history_pengajuan()
    {
        $list = array();
        if($this->ci->session->userdata('login_session')["role"] == "manager") {
            $list = $this->pengajuan->where_data("pengajuan", ["manager_approval_status" => "approve"]);
        }

        $data['title'] = "PENGAJUAN";
        $data['pengajuan'] = $list;

        $data['session'] = $this->ci->session->userdata('login_session');
        $this->template->load('templates/dashboard', 'pengajuan/history_pengajuan', $data);
    }

    public function finance_approve() {
        
        $data_body = array();
        if($this->ci->session->userdata('login_session')["role"] == "finance") {
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
    
            if (!$this->upload->do_upload('userfile')) {
                set_pesan('Pengajuan ' . $this->input->post('nama_barang') . ' gagal diapprove', false);
            } else {
                $uploaded_data = $this->upload->data();
                $data_body["finance_transfer_approval_photo"] = $uploaded_data['file_name'];
                $data_body["finance_approval_status"] = "approve";
                $data_body["finance_approval_status_timestamp"] = time();
                $id = $this->input->post("id_pengajuan");
                $save = $this->pengajuan->update($id, $data_body);
                if ($save) {
                    set_pesan('Pengajuan ' . $this->input->post('nama_barang') . ' berhasil diapprove.');
                } else {
                    set_pesan('Pengajuan ' . $this->input->post('nama_barang') . ' gagal diapprove', false);
                }
            }
        }

        redirect('pengajuan');
    }

    public function reject() {
        $data_body = array();
        $data_body["reject_reason"] = $this->input->post("alasan");
        if($this->ci->session->userdata('login_session')["role"] == "manager") {
            $data_body["manager_approval_status"] = "rejected";
            $data_body["manager_approval_status_timestamp"] = time();
        }

        if($this->ci->session->userdata('login_session')["role"] == "finance") {
            $data_body["finance_approval_status"] = "rejected";
            $data_body["finance_approval_status_timestamp"] = time();
        }

        $save = $this->pengajuan->update($this->input->post("id_pengajuan"), $data_body);
        if ($save) {
            set_pesan('Pengajuan berhasil ditolak.');
            redirect('pengajuan');
        } else {
            set_pesan('Pengajuan gagal ditolak', false);
            redirect('pengajuan');
        }

        exit;
    }

    public function approve($id) {
        $data_body = array();
        if($this->ci->session->userdata('login_session')["role"] == "manager") {
            $data_body["manager_approval_status"] = "approve";
            $data_body["manager_approval_status_timestamp"] = time();
            $data_body["tahap_pengajuan"] = "finance";
        }

        if($this->ci->session->userdata('login_session')["role"] == "finance") {
            $data_body["finance_approval_status"] = "approve";
            $data_body["finance_approval_status_timestamp"] = time();
        }

        $save = $this->pengajuan->update($id, $data_body);
        if ($save) {
            set_pesan('Pengajuan ' . $this->input->get('nama') . ' berhasil diapprove.');
            redirect('pengajuan');
        } else {
            set_pesan('Pengajuan ' . $this->input->get('nama') . ' gagal diapprove', false);
            redirect('pengajuan');
        }
    }

    public function add()
    {
        $this->form_validation->set_rules('nama_barang', 'Nama Barang', 'required|trim');
        $this->form_validation->set_rules('harga_barang', 'Harga Barang', 'required|trim|numeric');
        $this->form_validation->set_rules('kuantity_barang', 'Kuantity Barang', 'required|trim|numeric');
        if ($this->form_validation->run()) {
            $data_body = array();
            $data_body["nama_barang"] = $this->input->post("nama_barang");
            $data_body["harga_barang"] = $this->input->post("harga_barang");
            $data_body["kuantity_barang"] = $this->input->post("kuantity_barang");
            $data_body["link_pembelian_produk"] = $this->input->post("link_pembelian_produk");
            $data_body["link_video_produk"] = $this->input->post("link_video_produk");
            $data_body["tahap_pengajuan"] = "manager";

            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);
    
            $uploaded_data = array();
            if ($this->upload->do_upload('photo_file')) {
                $uploaded_data = $this->upload->data();
                $data_body["link_photo"] = base_url() . 'uploads/' . $uploaded_data['file_name']; // $this->input->post("link_photo");
            }

            $save = $this->pengajuan->insert_data($data_body);
            if ($save) {
                set_pesan('data berhasil disimpan.');
                redirect('pengajuan');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('pengajuan/add');
            }
        } else {
            $data['title'] = "PENGAJUAN";
            $this->template->load('templates/dashboard', 'pengajuan/add', $data);
        }
    }

    public function edit()
    {
        // $this->form_validation->run() == false
        if($_POST) {
            // var_dump($_POST);
            // $config['upload_path']          = './uploads/';
            // $config['allowed_types']        = 'gif|jpg|png';
            // $this->load->library('upload', $config);
            // $this->upload->do_upload('userfile');
            // $uploaded_data = $this->upload->data();

            $data_body = array();
            $data_body["nama_barang"] = $this->input->post("nama_barang");
            $data_body["link_photo"] = $this->input->post("link_photo");
            $data_body["harga_barang"] = $this->input->post("harga_barang");
            $data_body["kuantity_barang"] = $this->input->post("kuantity_barang");
            $data_body["link_pembelian_produk"] = $this->input->post("link_pembelian_produk");
            $data_body["link_video_produk"] = $this->input->post("link_video_produk");
            
            $config_upload['upload_path']          = './uploads/';
            $config_upload['allowed_types']        = 'gif|jpg|png';
            $config_upload['encrypt_name'] = TRUE;
            $this->upload->initialize($config_upload);
    
            $uploaded_data = array();
            if ($this->upload->do_upload('photo_file')) {
                $uploaded_data = $this->upload->data();
                $data_body["link_photo"] = base_url() . 'uploads/' . $uploaded_data['file_name']; // $this->input->post("link_photo");

                // delete old file
                $detail = $this->pengajuan->detail($this->input->post("id"));
                if(!empty(trim($detail["link_photo"]))) {
                    $explode_img = explode("/", $detail["link_photo"]);
                    if(file_exists(FCPATH . 'uploads/' . $explode_img[count($explode_img) - 1])) {
                        unlink(FCPATH . 'uploads/' . $explode_img[count($explode_img) - 1]);
                    }
                }
            }

            $save = $this->pengajuan->update($this->input->post("id"), $data_body);
            if ($save) {
                set_pesan('data berhasil disimpan.');
                redirect('pengajuan');
            } else {
                set_pesan('data gagal disimpan', false);
                redirect('pengajuan/edit?=' . $this->input->post("id"));
            }
        } else {
            $data['title'] = "EDIT PENGAJUAN";
            $data['detail'] = $this->pengajuan->detail($this->input->get("id"));
            $this->template->load('templates/dashboard', 'pengajuan/edit', $data);
        }
    }

    public function delete($getId)
    {
        $detail = $this->pengajuan->detail($getId);
        if ($this->pengajuan->delete($getId)) {
            if(!empty(trim($detail["link_photo"]))) {
                $explode_img = explode("/", $detail["link_photo"]);
                if(file_exists(FCPATH . 'uploads/' . $explode_img[count($explode_img) - 1])) {
                    unlink(FCPATH . 'uploads/' . $explode_img[count($explode_img) - 1]);
                }
            }

            set_pesan('data berhasil dihapus.');
        } else {
            set_pesan('data gagal dihapus.', false);
        }

        redirect('pengajuan');
    }
}
