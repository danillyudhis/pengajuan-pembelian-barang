<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pengajuan_model extends CI_Model
{
    public function list_data()
    {
        return $this->db->get('pengajuan')->result_array();
    }

    public function where_data($table, $where = null)
    {
        if($table == "pengajuan") {
            $this->db->order_by('id', 'DESC');
        }
        
        return $this->db->get_where($table, $where)->result_array();
    }

    public function insert_data($data)
    {
        return $this->db->insert('pengajuan', $data);
    }

    public function detail($id)
    {
        $this->db->where('id', $id);
        return $this->db->get("pengajuan")->row_array();
    }

    public function update($id, $data)
    {
        $this->db->where("id", $id);
        return $this->db->update("pengajuan", $data);
    }

    public function delete($id)
    {
        return $this->db->delete("pengajuan", ["id" => $id]);
    }
}
